﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            button1.Enabled = true;
            button2.Enabled = false;
            button3.Enabled = false;
            button4.Enabled = false;
            button5.Enabled = false;
            button7.Enabled = false;
            button15.Enabled = false;
            button16.Enabled = false;
            button6.Enabled = false;
            button17.Enabled = false;
            button18.Enabled = false;
            button19.Enabled = false;

        }

        private void button13_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Process.Start("http://www.Facebook.com");
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Process.Start("http://www.Twitter.com");
        }

        private void button10_Click(object sender, EventArgs e)
        {
            Process.Start("http://www.Instagram.com");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SidePanel.Height = button1.Height;
            SidePanel.Top = button1.Top;
            Form2 oform2 = new Form2();
            oform2.ShowDialog();
            button2.Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SidePanel.Height = button2.Height;
            SidePanel.Top = button2.Top;
            Form3 oform3 = new Form3();
            oform3.ShowDialog();
            if (oform3.x == 0)
            {
                button3.Enabled = true;
                button4.Enabled = true;
                button5.Enabled = true;
                button6.Enabled = true;
                button7.Enabled = true;
                button15.Enabled = true;
                button16.Enabled = true;
                button17.Enabled = true;
                button18.Enabled = true;
                button19.Enabled = true;
            }
            else
            {
                if (oform3.x == -1)
                {
                    button3.Enabled = false;
                    button4.Enabled = false;
                    button5.Enabled = false;
                    button6.Enabled = false;
                    button7.Enabled = false;
                    button15.Enabled = false;
                    button16.Enabled = false;
                    button17.Enabled = false;
                    button18.Enabled = false;
                    button19.Enabled = false;

                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            SidePanel.Height = button3.Height;
            SidePanel.Top = button3.Top;
            Form4 oform4 = new Form4();
            oform4.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            SidePanel.Height = button4.Height;
            SidePanel.Top = button4.Top;
            Form5 oform5 = new Form5();
            oform5.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            SidePanel.Height = button5.Height;
            SidePanel.Top = button5.Top;
            Form6 oform6 = new Form6();
            oform6.ShowDialog();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            SidePanel.Height = button7.Height;
            SidePanel.Top = button7.Top;
            Form7 oform7 = new Form7();
            oform7.ShowDialog();
        }

        private void button15_Click(object sender, EventArgs e)
        {
            SidePanel.Height = button15.Height;
            SidePanel.Top = button15.Top;
            Form8 oform8 = new Form8();
            oform8.ShowDialog();
        }

        private void button16_Click(object sender, EventArgs e)
        {
            SidePanel.Height = button16.Height;
            SidePanel.Top = button16.Top;
            Form9 oform9 = new Form9();
            oform9.ShowDialog();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            SidePanel.Height = button6.Height;
            SidePanel.Top = button6.Top;
            Form10 oform10 = new Form10();
            oform10.ShowDialog();
        }

        private void button17_Click(object sender, EventArgs e)
        {
            SidePanel.Height = button17.Height;
            SidePanel.Top = button17.Top;
            Form11 oform11 = new Form11();
            oform11.ShowDialog();
        }

        private void button18_Click(object sender, EventArgs e)
        {
            SidePanel.Height = button18.Height;
            SidePanel.Top = button18.Top;
            Form12 oform12 = new Form12();
            oform12.ShowDialog();
        }

        private void button19_Click(object sender, EventArgs e)
        {
            SidePanel.Height = button19.Height;
            SidePanel.Top = button19.Top;
            Form13 oform13 = new Form13();
            oform13.ShowDialog();
        }
    }
}
