﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace WindowsFormsApp1
{
    public partial class Form3 : Form
    {
        public int x = 0;

        public Form3()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void button13_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Process.Start("http://www.Facebook.com");
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Process.Start("http://www.Twitter.com");
        }

        private void button10_Click(object sender, EventArgs e)
        {
            Process.Start("http://www.Instagram.com");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string username;
            string pass;

            username = textBox1.Text;
            pass = textBox2.Text;
            if (username == "admin" && pass == "123")
            {
                MessageBox.Show("Logare cu succes");
                this.Close();
                x = 0;
            }
            else
            {
                MessageBox.Show("Eroare, incercati din nou");
                x = -1;
            }
        }
    }
}
