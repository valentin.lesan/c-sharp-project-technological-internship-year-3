﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Diagnostics;

namespace WindowsFormsApp1
{
    public partial class Form9 : Form
    {
        public Form9()
        {
            InitializeComponent();
        }

        public void load_DB()
        {
            //string data = textBox1.Text.ToString();
            //DateTime parsedDate = DateTime.ParseExact(data,"dd/mm/yyyy",null);
            string sql = "select * from v_loc_camera order by Loc_Camera desc";
            string constring = @"server=.\sqlexpress;
                                 database=Cazare;                   
                                 trusted_connection=true;";
            try
            {
                SqlConnection conn = new SqlConnection(constring);
                SqlDataAdapter da = new SqlDataAdapter(sql, conn);
                DataSet ds = new DataSet();
                da.Fill(ds, "v_loc_camera");
                DataTable dt = new DataTable();
                dt = ds.Tables["v_loc_camera"];
                dataGridView1.DataSource = dt;
                dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                dataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            }
            catch (Exception e)
            {

                MessageBox.Show("Error" + e);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            load_DB();
            button1.Enabled = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();
            // se creaza un WorkBook inauntrul Excel 
            Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);
            // se creaza o foaie Excelsheet in workbook  
            Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
            // se activeaza foaia Excelsheet peste program
            app.Visible = true;
            // se obtine referinta catre foaia ExcelSheet. Default, numele ei este Sheet1.  
            // depoziteaza referinta catre ExcelSheet  
            worksheet = workbook.Sheets["Sheet1"];
            worksheet = workbook.ActiveSheet;
            // Schimbam denumirea foii Excel  
            worksheet.Name = "Exported from gridview";
            // stocarea antetului in Excel  
            for (int i = 1; i < dataGridView1.Columns.Count + 1; i++)
            {
                worksheet.Cells[1, i] = dataGridView1.Columns[i - 1].HeaderText;
            }
            // stocarea fiecarei linii si coloane in excel sheet  
            for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
            {
                for (int j = 0; j < dataGridView1.Columns.Count; j++)
                {
                    worksheet.Cells[i + 2, j + 1] = dataGridView1.Rows[i].Cells[j].Value.ToString();
                }
            }
            // salvarea aplicatiei
            workbook.SaveAs("E:\\result.xls", Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Process.Start("http://www.Facebook.com");
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Process.Start("http://www.Twitter.com");
        }

        private void button10_Click(object sender, EventArgs e)
        {
            Process.Start("http://www.Instagram.com");
        }

        private void button13_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form9_Load(object sender, EventArgs e)
        {
            button1.Enabled = false;
        }
    }
}
