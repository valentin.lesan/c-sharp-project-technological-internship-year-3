﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Diagnostics;

namespace WindowsFormsApp1
{
    public partial class Form4 : Form
    {
        string connstring = @"server=.\SQLEXPRESS;
                                                 database=Cazare;
                                                 trusted_connection=true;";
        public Form4()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
       
        }

        private void Form4_Load(object sender, EventArgs e)
        {

            //Combobox 1 pentru turisti
            //dataset
            DataSet ds = new DataSet();

            //sqlquery
            string sqlquery1 = "select (rtrim(NumeTurist)+' '+ltrim(PrenumeTurist)) as Nume from v_Turisti order by idturist ";

            //data adapter
            SqlDataAdapter da = new SqlDataAdapter(sqlquery1, connstring);

            SqlCommandBuilder cmdbuilder = new SqlCommandBuilder(da);

            da.Fill(ds, "v_Turisti");

            comboBox1.ValueMember = "IdTurist";
            comboBox1.DisplayMember = "Nume";
            comboBox1.DataSource = ds.Tables[0];


            //Combobox 2 pentru camera

            DataSet ds2 = new DataSet();
            string sqlquery2 = "select NumarCamera from v_Camera";

            SqlDataAdapter da2 = new SqlDataAdapter(sqlquery2, connstring);
            SqlCommandBuilder cmdbuilder2 = new SqlCommandBuilder(da2);
            da2.Fill(ds2, "v_Camera");

            comboBox2.ValueMember = "IdCamera";
            comboBox2.DisplayMember = "NumarCamera";

            comboBox2.DataSource = ds2.Tables[0];
            // string selected_name;
            //selected_name="select idTurist from v_turisti where"+comboBox1.SelectedValue.ToString()+"" 
        }
        public void load_DB()
        {
            //string data = textBox1.Text.ToString();
            //DateTime parsedDate = DateTime.ParseExact(data,"dd/mm/yyyy",null);
            string sql = "select * from inregistrare";
            string constring = @"server=.\sqlexpress;
                                 database=Cazare;                   
                                 trusted_connection=true;";
            try
            {
                SqlConnection conn = new SqlConnection(constring);
                SqlDataAdapter da = new SqlDataAdapter(sql, conn);
                DataSet ds = new DataSet();
                da.Fill(ds, "inregistrare");
                DataTable dt = new DataTable();
                dt = ds.Tables["inregistrare"];
                dataGridView1.DataSource = dt;
                dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                dataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            }
            catch (Exception e)
            {

                MessageBox.Show("Error" + e);
            }
        }

        public void usp()
        {
            try
            {
                int local = comboBox1.SelectedIndex;
                int local2 = comboBox2.SelectedIndex;
                //valoare idcamera
                local2++;
                //valoare idturist
                local++;
                SqlConnection conn = new SqlConnection(connstring);
                conn.Open();
                SqlCommand s = new SqlCommand("select max(idinregistrare)+1 from inregistrare", conn);
                Int32 ress = (Int32)s.ExecuteScalar();
                SqlCommand cmd = new SqlCommand("usp_insertTourist", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IdInregistrare", SqlDbType.Char).Value = ress.ToString();
                cmd.Parameters.AddWithValue("@IdCamera", SqlDbType.Char).Value = local2.ToString();
                cmd.Parameters.AddWithValue("@IdTurist", SqlDbType.Char).Value = local.ToString(); ;
                cmd.Parameters.AddWithValue("@Data_inchirierii", SqlDbType.Date).Value = dateTimePicker1.Value;
                cmd.Parameters.AddWithValue("@Termen_Cazare", SqlDbType.Int).Value = numericUpDown1.Value;
                cmd.Parameters.AddWithValue("@Termen_adaos", SqlDbType.Int).Value = numericUpDown2.Value;
                cmd.Parameters.Add("@rez", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show("Error" + e);
                throw;
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
  
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Process.Start("http://www.Facebook.com");
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Process.Start("http://www.Twitter.com");
        }

        private void button10_Click(object sender, EventArgs e)
        {
            Process.Start("http://www.Instagram.com");
        }

        private void button13_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            usp();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            load_DB();
        }
    }
}
