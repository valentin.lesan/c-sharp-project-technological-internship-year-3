﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Diagnostics;

namespace WindowsFormsApp1
{
    public partial class Form12 : Form
    {
        public Form12()
        {
            InitializeComponent();
        }

        public void load_DB()
        {
            //string data = textBox1.Text.ToString();
            //DateTime parsedDate = DateTime.ParseExact(data,"dd/mm/yyyy",null);
            string sql = @"select Facilitate,[Pret/Zi] as Pret_sejur from camera
                            inner join camera_type on
                            camera.idcamera_type = camera_type.idcamera_type
                            where camera_type.[pret/zi]<=90
                            group by facilitate,[pret/zi]
                                    union all
                            select facilitate,[Pret/Zi] as Pret_sejur from camera
                              inner join camera_type on
                              camera.idcamera_type=camera_type.idcamera_type
                              where camera_type.[pret/zi] between '91' and '110'
                            group by facilitate, [pret/zi]
                            union all
                              select facilitate, [Pret/Zi] as Pret_sejur from v_Camera
                              inner join v_Camera_type on
                              v_Camera.idcamera_type= v_Camera_type.idcamera_type
                              where v_Camera_type.[pret/zi] between '110' and '130'
                            group by facilitate, [pret/zi]
                            union all
                              select facilitate, [Pret/Zi] as Pret_sejur from v_Camera
                              inner join v_Camera_type on
                              v_Camera.idcamera_type= v_Camera_type.idcamera_type
                              where v_Camera_type.[pret/zi] between '130' and '200'
                            group by facilitate, [pret/zi]";

            string constring = @"server=.\sqlexpress;
                                 database=Cazare;                   
                                 trusted_connection=true;";
            try
            {
                SqlConnection conn = new SqlConnection(constring);
                SqlDataAdapter da = new SqlDataAdapter(sql, conn);
                DataSet ds = new DataSet();
                da.Fill(ds, "Camera");
                DataTable dt = new DataTable();
                dt = ds.Tables["Camera"];
                dataGridView1.DataSource = dt;
                dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                dataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            }
            catch (Exception e)
            {

                MessageBox.Show("Error" + e);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            load_DB();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Process.Start("http://www.Facebook.com");
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Process.Start("http://www.Twitter.com");
        }

        private void button10_Click(object sender, EventArgs e)
        {
            Process.Start("http://www.Instagram.com");
        }

        private void button13_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
