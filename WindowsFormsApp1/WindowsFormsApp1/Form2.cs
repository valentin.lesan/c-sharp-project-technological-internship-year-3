﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;

namespace WindowsFormsApp1
{
    public partial class Form2 : Form
    {
        int _charIndex = 0;
        int _charIndex2 = 0;
        string _text = "admin";
        string _text2 = "123";

        public Form2()
        {
            InitializeComponent();
        }

        private void button13_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }
        private void TypewriteText()
        {
            while (_charIndex < _text.Length)
            {
                Thread.Sleep(300);
                label4.Invoke(new Action(() =>
                {
                    label4.Text += _text[_charIndex];
                }));
                _charIndex++;
            }
        }
        private void TypewriteText2()
        {
            while (_charIndex2 < _text2.Length)
            {

                Thread.Sleep(300);
                label5.Invoke(new Action(() =>
                {
                    label5.Text += _text2[_charIndex2];
                }));
                _charIndex2++;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            _charIndex = 0;
            _charIndex2 = 0;
            label4.Text = "";
            label5.Text = "";
            Thread t = new Thread(new ThreadStart(this.TypewriteText));
            t.Start();
            Thread t2 = new Thread(new ThreadStart(this.TypewriteText2));
            t2.Start();

        }
    }
}
