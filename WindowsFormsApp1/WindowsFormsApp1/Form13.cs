﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Diagnostics;

namespace WindowsFormsApp1
{
    public partial class Form13 : Form
    {
        public Form13()
        {
            InitializeComponent();
        }

        public void load_DB()
        {
            //string data = textBox1.Text.ToString();
            //DateTime parsedDate = DateTime.ParseExact(data,"dd/mm/yyyy",null);
            string sql = @"select * from v_CALCUL_Total order by IdTurist";
            string constring = @"server=.\sqlexpress;
                                 database=Cazare;                   
                                 trusted_connection=true;";
            try
            {
                SqlConnection conn = new SqlConnection(constring);
                SqlDataAdapter da = new SqlDataAdapter(sql, conn);
                DataSet ds = new DataSet();
                da.Fill(ds, "v_CALCUL_Total");
                DataTable dt = new DataTable();
                dt = ds.Tables["v_CALCUL_Total"];
                dataGridView1.DataSource = dt;
                dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                dataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            }
            catch (Exception e)
            {

                MessageBox.Show("Error" + e);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            load_DB();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Process.Start("http://www.Facebook.com");
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Process.Start("http://www.Twitter.com");
        }

        private void button10_Click(object sender, EventArgs e)
        {
            Process.Start("http://www.Instagram.com");
        }

        private void button11_Click(object sender, EventArgs e)
        {

        }

        private void button13_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
